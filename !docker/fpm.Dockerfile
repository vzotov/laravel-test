FROM php:8.0.2-fpm

#ADD docker/conf/www.conf /usr/local/etc/php-fpm.d/www.conf

#RUN addgroup -g 1000 laravel && adduser -G laravel -g laravel -s /bin/sh -D laravel

#RUN mkdir -p /var/www/html

#RUN chown laravel:laravel /var/www/html
# Install selected extensions and other stuff
RUN apt update \
&& apt install -y libpq-dev \
&& docker-php-ext-install pdo pdo_pgsql
