<?php

namespace Tests\Unit;

use Tests\TestCase;

class UsersTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');
        $this->assertEquals(200, $response->status());
    }

    public function testAuthorize()
    {
        $response = $this->post('/login', [
            'email' => 'test2@test.te',
            'password' => 'test223456',
        ]);
        $this->assertEquals(302, $response->status());
    }

}
